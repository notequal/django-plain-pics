# Django package imports.
from django.core.files import File
from django.core.management.base import BaseCommand as BC

# Local package imports.
from plainpics.models import Pic
from plainpics.models import Tag


class Command(BC):

    def __set_tags(self, pic, tags):

        for tag in tags:

            try:
                pic.tags.add(tag)

            except Exception as e:
                self.stderr.write(str(e))

        return None

    def add_arguments(self, parser):

        parser.add_argument('--upload_to', 
            type = str, 
            default = None, 
            help = 'specifies a path and file name relative to the ' \
                + '\"MEDIA_ROOT\" location where the image will be saved.')
        parser.add_argument('--tags', 
            nargs = '+', 
            default = [], 
            help = 'list of tags to attach to the uploaded image')
        parser.add_argument('filename', 
            type = str, 
            help = 'upload the image specified by the given path')

        return None

    def handle(self, *args, **options):

        upload_to = options.get('upload_to')
        tag_names = options.get('tags', [])
        filename = options.get('filename')

        # There is probably a way better way to handle the image tags, 
        # however, this will do for now.
        tags = []

        for name in tag_names:

            try:

                tag = Tag.objects.get(name = name)
                tags.append(tag)

            except:

                tag = Tag(name = name)
                tag.save()
                tags.append(tag)

        # Create the Pic instance and open the file.  If we override 
        # the default location where images are uploaded, then set the 
        # attribute in the Pic instance.
        pic = Pic()
        pic.data = File(open(filename, 'rb'))

        if upload_to is not None:
            setattr(pic, 'upload_to', upload_to)

        # Save the image, then set the tags.  Get the URL to the 
        # image if uploading was successful.
        url = ''

        try:

            pic.save()
            self.__set_tags(pic, tags)
            url = pic.data.url

        except Exception as e:
            self.stderr.write(str(e))

        return url


