# Standard library imports.
import datetime
import os
import uuid

# Django package imports.
from django.conf import settings
from django.db import models


def upload_to(instance, filename):

    # Extract the name of the file without any path information.
    basename = os.path.basename(filename)

    # Get the location where the image will be uploaded.  If the Pic 
    # instance has an "upload_to" attribute, then that will be 
    # used as the path relative to the "MEDIA_ROOT" setting.
    override_path = getattr(instance, 'upload_to', None)
    plainpic_path = getattr(settings, 'PLAINPICS_PATH', 'plainpics/')

    if not override_path:
        plainpic_path += datetime.datetime.utcnow().strftime('%Y/%m/%d/') \
            + basename

    else:
        plainpic_path = override_path

    # Clean the first slash from the plainpic_path.
    if plainpic_path.startswith('/'):
        plainpic_path = plainpic_path[1:]

    if override_path:
        if os.path.isfile(settings.MEDIA_ROOT + plainpic_path):
            raise Exception('file already exists')

    return plainpic_path


class Pic(models.Model):

    _id = models.UUIDField(
        primary_key = True, default = uuid.uuid4, editable = False)
    data = models.ImageField(upload_to = upload_to)
    comments = models.TextField(blank = True, null = True)
    date_uploaded = models.DateTimeField(auto_now_add = True)
    last_modified = models.DateTimeField(auto_now = True)
    tags = models.ManyToManyField('Tag', blank = True)


class Tag(models.Model):

    _id = models.UUIDField(
        primary_key = True, default = uuid.uuid4, editable = False)
    name = models.CharField(max_length = 64, unique = True)



