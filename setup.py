# Standard library imports.
import os

# Setuptools package imports.
from setuptools import setup

# Read the README.rst file for the 'long_description' argument given 
# to the setup function.
README = open(os.path.join(os.path.dirname(__file__), 'README.rst')).read()

# Allow setup.py to be run from any path.
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))


setup(
    name = 'Django-Plain-Pics', 
    version = '0.0.1a1', 
    packages = [
        'plainpics', 
        'plainpics.management', 
        'plainpics.management.commands', 
        'plainpics.migrations'], 
    install_requires = [
        'Django <= 3.0.0', 
        'Pillow <= 7.0.0'],  
    license = 'BSD License', 
    description = 'Store images with optional tags.', 
    long_description = README, 
    url = 'https://bitbucket.org/notequal/django-plain-pics/', 
    author = 'Stanley Engle', 
    author_email = 'stan.engle@gmail.com', 
    classifiers = [
        'Development Status :: 2 - Pre-Alpha', 
        'Environment :: Web Environment', 
        'Framework :: Django', 
        'Framework :: Django :: 2.2', 
        'Intended Audience :: End Users/Desktop', 
        'License :: OSI Approved :: BSD License', 
        'Natural Language :: English', 
        'Operating System :: OS Independent', 
        'Programming Language :: Python', 
        'Programming Language :: Python :: 3.7', 
        'Topic :: Internet :: WWW/HTTP', 
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content'],)


